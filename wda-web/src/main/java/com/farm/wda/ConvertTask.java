package com.farm.wda;

import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import com.farm.wda.adaptor.DocConvertorBase;
import com.farm.wda.domain.DocTask;
import com.farm.wda.util.ConfUtils;
import com.farm.wda.util.FileUtil;

/**
 * 文档转换调度任务（可定时）
 * 
 * @author wangdong
 *
 */
public class ConvertTask implements Callable<Boolean> {
	private static final Logger log = Logger.getLogger(ConvertTask.class);
	private DocTask task;

	public ConvertTask(DocTask task) {
		this.task = task;
	}

	@Override
	public Boolean call() throws Exception {
		log.info("WDA[fileConvert]------------converting..."+task.getFile().getName());
		FileUtil.wirteLog(task.getLogFile(), "[start] converting ... ...");
		DocConvertorBase convertor = ConfUtils.getConvertor(task.getFileTypeName(), task.getTargettype());
		convertor.convert(task.getFile(), task.getFileTypeName(), task.getTargetFile(), task);
		return true;
	}

}
